function zoomIn(id, callback, fadeOut) {
    TweenLite.to(id, .25, {
        scaleX: 1,
        scaleY: 1,
        alpha: 1,
        // ease: Cubic.easeIn,
        onComplete: function() {
            TweenLite.to(id, 2, {
                scaleX: .9,
                scaleY: .9
            // ,ease: Cubic.easeOut,
            });
            fadeOut = typeof fadeOut !== 'undefined' ? fadeOut : true;
            TweenLite.to(id, .5, {
                delay: 1.5,
                alpha: fadeOut === true ? 0 : 1,
                overwrite: 0,
                ease: Cubic.easeOut,
                onComplete: function() {

                    if (typeof callback == "function") {
                        callback();
                    }
                }
            });
        }
    });
}

function getImageBounds(img) {
    var w = img.image.width;
    var h = img.image.height;

    ctx.drawImage(img.image, 0, 0, w, h);
    // ctx.createImageData(w, h)

    var idata = ctx.getImageData(0, 0, w, h),
        buffer = idata.data,
        buffer32 = new Uint32Array(buffer.buffer),
        x,
        y,
        x1 = w,
        y1 = h,
        x2 = 0,
        y2 = 0;

    // get left edge
    for (y = 0; y < h; y++) {
        for (x = 0; x < w; x++) {
            if (buffer32[x + y * w] > 0) {
                if (x < x1)
                    x1 = x;
            }
        }
    }

    // get right edge
    for (y = 0; y < h; y++) {
        for (x = w; x >= 0; x--) {
            if (buffer32[x + y * w] > 0) {
                if (x > x2)
                    x2 = x;
            }
        }
    }

    // get top edge
    for (x = 0; x < w; x++) {
        for (y = 0; y < h; y++) {
            if (buffer32[x + y * w] > 0) {
                if (y < y1)
                    y1 = y;
            }
        }
    }

    // get bottom edge
    for (x = 0; x < w; x++) {
        for (y = h; y >= 0; y--) {
            if (buffer32[x + y * w] > 0) {
                if (y > y2)
                    y2 = y;
            }
        }
    }

    var rectObj = {
        x: x1,
        y: y1,
        w: x2 - x1,
        h: y2 - y1
    };
    img.regX = rectObj.x + (rectObj.w / 2);
    img.regY = rectObj.y + (rectObj.h / 2);
    img.x = stage.canvas.width / 2; //rectObj.x + (rectObj.w / 2);
    img.y = rectObj.y + (rectObj.h / 2); //rectObj.y + (rectObj.y);


    // console.log(rectObj.x, rectObj.y, rectObj.w, rectObj.h)


    ctx.clearRect(0, 0, w, h)
//return rectObj;
}

