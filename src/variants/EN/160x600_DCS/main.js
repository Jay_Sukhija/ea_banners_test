// // variables:
var frame1_Copy,
    frame2_Copy,
    frame3_Copy,
    main_bg,
    xbox_logo,
    bf_logo,
    diceea_lockup,
    smoke,
    cta;

//manifest - mandatory object      
function setupManifest() {
    manifest = [
        {
            src: "./" + pathCheck + "bg.jpg",
            id: 'bg',
            type: createjs.LoadQueue.IMAGE
        },
        {
            src: "./" + pathCheck + "cta.png",
            id: 'cta',
            type: createjs.LoadQueue.IMAGE
        },
        {
            src: "./" + pathCheck + "xbox_logo.png",
            id: 'xbox_logo',
            type: createjs.LoadQueue.IMAGE
        },
        {
            src: "./" + pathCheck + "text_1.png",
            id: 'text_1',
            type: createjs.LoadQueue.IMAGE
        },
        {
            src: "./" + pathCheck + "text_2.png",
            id: 'text_2',
            type: createjs.LoadQueue.IMAGE
        },
        {
            src: "./" + pathCheck + "text_3.png",
            id: 'text_3',
            type: createjs.LoadQueue.IMAGE
        },
        {
            src: "./" + pathCheck + "bf_logo.png",
            id: 'bf_logo',
            type: createjs.LoadQueue.IMAGE
        },
        {
            src: "./" + pathCheck + "diceea_lockup.svg",
            id: 'diceea_lockup',
            type: createjs.LoadQueue.IMAGE
        },
        {
            src: "./" + pathCheck + "smoke.png",
            id: 'smoke',
            type: createjs.LoadQueue.IMAGE
        }

    ];
}


//numOfLoops = 3; // - can be overwritten here | default is 3

// already added to stage are Containers:middleContainer, bottomContainer, topContainer
// USAGE: middleContainer.addChild(childName)

//*******************************************
// ***** add custom assets:
//*******************************************
function addCustomAssets() {
    // all visual graphics:
    main_bg = new createjs.Bitmap(loadedImages['bg']);

    xbox_logo = new createjs.Bitmap(loadedImages['xbox_logo']);
    xbox_logo.y = xbox_logo.image.height;

    cta = new createjs.Bitmap(loadedImages['cta']);
    cta.alpha = 0;
    cta.scaleX = cta.scaleY = .2;

    frame1_Copy = new createjs.Bitmap(loadedImages['text_1']);
    frame2_Copy = new createjs.Bitmap(loadedImages['text_2']);
    frame3_Copy = new createjs.Bitmap(loadedImages['text_3']);
    bf_logo = new createjs.Bitmap(loadedImages['bf_logo']);
    diceea_lockup = new createjs.Bitmap(loadedImages['diceea_lockup']);
    diceea_lockup.scaleX = diceea_lockup.scaleY = .25;
    diceea_lockup.alpha = 0;

    smoke = new createjs.Bitmap(loadedImages['smoke']);
    smoke.x = stage.canvas.width - smoke.image.width;
    smoke.y = stage.canvas.height - smoke.image.height;

    bottomContainer.addChild(main_bg, smoke);
    middleContainer.addChild(frame1_Copy, frame2_Copy, frame3_Copy, bf_logo, diceea_lockup, cta);
    topContainer.addChild(xbox_logo);

    getImageBounds(frame1_Copy);
    getImageBounds(frame2_Copy);
    getImageBounds(frame3_Copy);
    getImageBounds(bf_logo);
    getImageBounds(cta);

    diceea_lockup.y = cta.y - 20;
    diceea_lockup.x = cta.x + 59;

    TweenLite.set([frame1_Copy, frame2_Copy, frame3_Copy, bf_logo], {
        alpha: 0,
        scaleX: 4,
        scaleY: 4
    });


    frame_1_anim();
}

//*******************************************
// ***** add custom animations:
//*******************************************
function frame_1_anim() {
    TweenLite.to(smoke, 10, {
        x: 0,
        ease: Linear.easeNone
    });
    zoomIn(frame1_Copy, frame_2_anim);
}

function frame_2_anim() {
    zoomIn(frame2_Copy, frame_3_anim);
}

function frame_3_anim() {
    zoomIn(frame3_Copy, frame_4_anim);
}

function frame_4_anim() {
    zoomIn(bf_logo, null, false);
    TweenLite.to(cta, .8, {
        alpha: 1,
        scaleX: 1,
        scaleY: 1,
        delay: .5,
        ease: Cubic.easeInOut,
        onComplete: setupRollStates
    });
    TweenLite.to(xbox_logo, 1, {
        y: 0,
        delay: .7,
        ease: Cubic.easeInOut
    });
    TweenLite.to(diceea_lockup, 1, {
        alpha: 1,
        delay: .7,
        ease: Cubic.easeOut
    });
}

function setupRollStates() {
    document.getElementById('exitButton').addEventListener('mouseover', onMouseOver);
    document.getElementById('exitButton').addEventListener('mouseout', onMouseOut);
}

function onMouseOver() {
    TweenLite.to(cta, .4, {
        scaleX: 1.1,
        scaleY: 1.1,
        ease: Cubic.easeOut
    });
}

function onMouseOut() {
    TweenLite.to(cta, .4, {
        scaleX: 1,
        scaleY: 1,
        ease: Cubic.easeIn
    });
}

//*******************************************
// ***** check for loops:
//*******************************************
function checkForLoops() {
    loopCount++;
    if (loopCount < numOfLoops) {
        TweenLite.delayedCall(2, loopFrame);
    }
}

function loopFrame() {


}


